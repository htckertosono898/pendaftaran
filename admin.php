<?php
 session_start();

 if( !isset ($_SESSION["login"]) ){
     header("location: logins.php");
      exit;
  }
require 'function.php';

$mahasiswa = query ("SELECT * FROM mahasiswa");
// jika tombol cari  di klik maka seluruh mahasiswa akan di timpa
if( isset($_POST["cari"]) ) {
   $mahasiswa = cari($_POST["keyword"]);

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>halaman admin</title>
    <link rel="stylesheet" href="zanuar.css/index.css">
</head>
<body>
    <h1 align="middle">Daftar Siswa</h1>
    <a href="logout.php">log out</a> <br>
    <a href="ubah_pengumuman.php">buat/edit pengumuman</a> <br>
    <a href="registrasi.php">tambah admin</a>
 <br>
 <br>
<form action="" method="post">

<input type="text" name="keyword" size="30" autofocus placeholder="masukkan keyword pencarian" autocomplete="off">
<button type="submit" name="cari">cari</button>

</form> <br>
<table border="1" cellpadding="10" cellcpasing="0">
    <tr>
        <th>no.</th>
        <th>NAMA</th>
        <th>NIK</th>
        <th>ALAMAT</th>
        <th>JENIS KELAMIN</th>
        <th>NO_HP</th>
        <th>EMAIL</th>
        <th>AKSI</th>
    </tr>
    <?php $i = 1;  ?>
    <?php foreach ($mahasiswa as $row) : ?>
    <tr >
        <td> <?php echo $i; ?></td>
        <td><?php echo $row["nama"] ?></td>
        <td><?php echo $row["nisn"] ?></td>
        <td><?php echo $row["alamat"] ?></td>
        <td><?php echo $row["jenis_kelamin"] ?></td>
        <td><?php echo $row["no_hp"] ?></td>
        <td><?php echo $row["email"] ?></td>
        <td><a href="ubah.php?id=<?= $row["id"]; ?>">ubah</a> |
          <a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin akan menghapus data?')">hapus</a>
        </td>
        
    </tr>
    <?php  $i++;  ?>
    <?php endforeach;  ?>
    </table>
</body>
</html>